<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Mail\NewContactMail;
use Mail;

class ContactMail extends Model
{
    /*
    |---------------------------------------------
    | CONTACT MAIL
    |---------------------------------------------
    */
    public function sendContactMail($payload){
    	$name 	 = $payload->name;
    	$email   = $payload->email;
    	$phone   = $payload->phone;
    	$message = $payload->message;


    	$mail_data = [
    		'name' 		=> $name,
    		'email' 	=> $email,
    		'phone' 	=> $phone,
    		'message' 	=> $message,
    	];

    	\Mail::to(env("ADMIN_EMAIL"))->send(new NewContactMail($mail_data));
		
        $data =[
			'status' 	=> 'success',
			'message' 	=> 'Thanks for contacting us, we will get back to you as soon as possible.',
		];

		// return
		return $data;
    }
}
