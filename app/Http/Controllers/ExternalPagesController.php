<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ExternalPagesController extends Controller
{
    /*
    |---------------------------------------------
    | SHOW INDEX PAGES VIEW
    |---------------------------------------------
    */
    public function index(){
    	// return
    	return view('pages.index');
    }

    /*
    |---------------------------------------------
    | SHOW INDEX PAGES VIEW
    |---------------------------------------------
    */
    public function about(){
    	// return
    	return view('pages.about');
    }


    /*
    |---------------------------------------------
    | SHOW INDEX PAGES VIEW
    |---------------------------------------------
    */
    public function product(){
    	// return
    	return view('pages.product');
    }

    /*
    |---------------------------------------------
    | SHOW INDEX PAGES VIEW
    |---------------------------------------------
    */
    public function contact(){
    	// return
    	return view('pages.contact');
    }

    /*
    |---------------------------------------------
    | SHOW INDEX PAGES VIEW
    |---------------------------------------------
    */
    public function policy(){
    	// return
    	return view('pages.policy');
    }

    /*
    |---------------------------------------------
    | SHOW INDEX PAGES VIEW
    |---------------------------------------------
    */
    public function terms(){
    	// return
    	return view('pages.terms');
    }
}
