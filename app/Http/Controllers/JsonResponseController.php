<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ContactMail;

class JsonResponseController extends Controller
{
    /*
    |---------------------------------------------
    | SEND MAIL
    |---------------------------------------------
    */
    public function sendMail(Request $request){
    	$mail = new ContactMail();
    	$data = $mail->sendContactMail($request);


    	// return response
    	return response()->json($data);
    }
}
