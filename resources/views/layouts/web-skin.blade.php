
<!DOCTYPE html>
<html lang="en" class="wide smoothscroll wow-animation">
<head>
    <!-- Site Title -->
    <title>@yield('title')</title>
    <meta name="format-detection" content="telephone=no"/>
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=0"/>

    <meta name="description" content="Godsfinger Welding is the most economical and efficient way to join metals permanently. It is the only way of joining two or more pieces of metal to make them act as a single piece. Visit us today for all your Welding and Crafting." />
    <meta name="keywords" content="Pneumatic Engineers Hydraulic Engineers Process Engineers Stainless Steel Pipework Carbon Steel Pipework Installation Pipework Fabrication" />
    <meta name="author" content="E-Devs Limited" />
    <!-- Facebook and Twitter integration -->
    <meta property="og:title" content="Godsfinger Welding"/>
    <meta property="og:image" content="{{env("APP_URL")}}/images/page-02_img02.jpg"/>
    <meta property="og:url" content="{{env("APP_URL")}}"/>
    <meta property="og:site_name" content="GodsFinger"/>
    <meta property="og:description" content="Connecting Metals, joining two or more pieces of metal to make them act as a single piece."/>

    {{-- twitter --}}
    <meta name="twitter:title" content="Godsfinger Welding" />
    <meta name="twitter:image" content="{{env("APP_URL")}}/images/page-02_img02.jpg" />
    <meta name="twitter:url" content="{{env("APP_URL")}}" />
    <meta name="twitter:card" content="Connecting Metals, joining two or more pieces of metal to make them act as a single piece." />

    <!-- Stylesheets -->
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <link href='//fonts.googleapis.com/css?family=Rubik+One%7CLato:400,900' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!--[if lt IE 10]>
    <script src="js/html5shiv.min.js"></script>
    <![endif]-->
</head>
<body>
    <input type="hidden" id="token" value="{{ csrf_token() }}" name="">
    <!-- The Main Wrapper -->
    <div class="page">

        <!--For older internet explorer-->
        <div class="old-ie" style='background: #212121; padding: 10px 0; box-shadow: 3px 3px 5px 0 rgba(0,0,0,.3); clear: both; text-align:center; position: relative; z-index:1;'>
            <a href="javascript:void(0);">
                <img src="images/ie8-panel/warning_bar_0000_us.jpg" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today."/>
            </a>
        </div>
        <!--END block for older internet explorer-->

        <!--========================================================
                                  HEADER
        =========================================================-->
        <header class="page-header">
            <!-- RD Navbar -->
            <div class="rd-navbar-wrap">
                <nav class="rd-navbar" data-rd-navbar-lg="rd-navbar-static">
                    <div class="rd-navbar-inner">
                        <!-- RD Navbar Panel -->
                        <div class="rd-navbar-panel">

                            <!-- RD Navbar Toggle -->
                            <button class="rd-navbar-toggle" data-rd-navbar-toggle=".rd-navbar"><span></span></button>
                            <!-- END RD Navbar Toggle -->

                            <!-- RD Navbar Brand -->
                            <div class="rd-navbar-brand">
                                <a href="{{ url('/') }}" class="brand-name">Godsfinger Engineering</a>
                            </div>
                            <!-- END RD Navbar Brand -->

                            <!-- RD Navbar Toggle -->
                            <button class="rd-collapse-toggle" data-rd-navbar-toggle=".rd-navbar-info"><span></span></button>
                            <!-- END RD Navbar Toggle -->
                        </div>
                        <!-- END RD Navbar Panel -->

                        <div class="rd-navbar-nav-wrap">

                            <!-- RD Navbar Nav -->
                            <ul class="rd-navbar-nav">
                                <li class="active"><a href="{{ url('/') }}">Home</a></li>
                                <li>
                                    <a href="{{ url('about') }}">About</a>
                                </li>
                                <li><a href="{{ url('product') }}">Products</a></li>
                                <li><a href="{{ url('contact') }}">Contacts</a></li>
                            </ul>
                            <!-- END RD Navbar Nav -->
                        </div>

                        <dl class="rd-navbar-info" style="font-size: 14px;">
                            <dt>Call us:</dt>
                            <dd>
                                <a class="text-primary hover-effect-2" href="callto:#">+2347032378215</a>, 
                                <a class="text-primary hover-effect-2" href="callto:#">+2348177273900</a>
                            </dd>
                        </dl>
                    </div>
                </nav>
            </div>
            <!-- END RD Navbar -->
        </header>

        <!--========================================================
                                  CONTENT
        =========================================================-->
        <main class="page-content">
            @yield('contents')
        </main>

        <!--========================================================
                                  FOOTER
        ==========================================================-->
        <footer class="page-footer bg-grayscale-default text-center text-md-left">
            <div class="container">
                <div class="row">
                    <div class="col-xs-6 col-md-3">
                        <!-- RD Navbar Brand -->
                        <div class="rd-navbar-brand">
                            <a href="{{ url('/') }}" class="brand-name">GodsFinger Engineering</a>
                        </div>
                        <!-- END RD Navbar Brand -->

                        <p class="text-primary offset-1">© <span id="copyright-year"></span> Developed By E-Devs Limited<br> All rights reserved.</p>
                    </div>

                    <div class="col-xs-6 col-md-2">
                        <h6>Menu</h6>
                        <ul class="list offset-1">
                            <li class="active"><a href="{{ url('/') }}">Home</a></li>
                            <li><a href="{{ url('about') }}">About</a></li>
                            <li><a href="{{ url('product') }}">Products</a></li>
                            <li><a href="{{ url('contact') }}">Contacts</a></li>
                        </ul>
                    </div>

                    <div class="col-sm-12 col-md-4">
                        <div class="row">
                            <div class="col-xs-6">
                                <h6>Social</h6>
                                <ul class="list offset-1">
                                    <li><a href="#">Facebook<span class="fa-facebook"></span></a></li>
                                    <li><a href="#">LinkedIn<span class="fa-linkedin"></span></a></li>
                                </ul>
                            </div>

                            <div class="col-xs-6 offset-2">

                                <!-- RD Navbar Search -->
                                <div class="rd-navbar-search">
                                    <form class="rd-navbar-search-form" action="search.php" method="GET">
                                        <label class="rd-navbar-search-form-input">
                                            <input type="text" name="s" placeholder="Search" autocomplete="off"/>
                                        </label>
                                        <button class="rd-navbar-search-form-submit" type="submit"></button>
                                    </form>
                                </div>
                                <!-- END RD Navbar Search -->

                            </div>

                            <div class="col-xs-12 offset-2">
                                <a href="{{ url('terms') }}">Terms of use</a> <span class="text-primary">|</span> 
                                <a href="{{ url('policy') }}">Privacy Policy</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </div>

    <!-- Core Scripts -->
    <script src="js/core.min.js"></script>
    <!-- Additional Functionality Scripts -->
    <script src="js/script.js"></script>

    @yield('scripts')
</body>
</html>