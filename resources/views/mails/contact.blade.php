<!DOCTYPE html>
<html>
<head>
	<title>New Contact Message</title>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-beta/js/bootstrap.min.js"></script>
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootswatch/3.3.7/cerulean/bootstrap.min.css">
	<link href='https://fonts.googleapis.com/css?family=Amiko' rel='stylesheet'>
	<style type="text/css">
		body {
		    font-family: 'Amiko';font-size: 12px;
		}
	</style>
</head>
<body>
<div class="container">
	<div class="row">
		<div class="col-md-6 col-md-offset-2">

			<h2 class="lead">New Contact Message</h2>
			<p>
				Hello GodsFinger,
			</p>
			<p> 
				<b>{{ $data['name'] }}, has sent you a message on your website.<br />
				See details below.
				<br /><br />

				<div class="panel panel-default">
					<div class="panel-heading"><h4>Details</h4></div>
					<div class="panel-body">
						<table class="table">
							<tr>
								<td>Name</td>
								<td>{{ $data['name'] }}</td>
							</tr>
							<tr>
								<td>Email</td>
								<td>{{ $data['email'] }}</td>
							</tr>
							<tr>
								<td>Phone</td>
								<td>{{ $data['phone'] }}</td>
							</tr>
						</table>
						<br />
						{{ $data['message'] }}
					</div>
				</div>
			</p>
			<br />
			<div class="features"><!-- Main Points -->
				<i class="line-font blue icon-shield"></i><!-- Main Point Icon -->
				<hr />
				<p class="small">
					Get more upgrade Today contact E-Devs Technology.
				</p><!-- Main Text -->
			</div><!-- End Main Points -->
			<hr />
			<div class="features small">
				<i class="line-font blue icon-bar-chart"></i>
				<p>Techinal Support: Ekpoto Liberty</p>
				<p>Office: 08127160258, 09092367163 </p>
			</div>
		</div>
	</div>
</div>
</body>
</html>