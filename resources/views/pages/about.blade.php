@extends('layouts.web-skin')

{{--  title --}}
@section('title')
    GodsFinger Engineering | Products
@endsection

{{--  contents --}}
@section('contents')
    <!-- For all your welding needs -->
        <section class="well-sm relative">
            <div class="container">
                <div class="row">
                    <div class="col-md-preffix-7 col-md-5">
                        <h2>For all your welding needs</h2>
                        <p>As the leader of the industry, GodsFinger Engineering has been Nigeria’s top manufacturer of Metalic services for constructions, metal crafts and weldering, consumables for the past half century.</p>
                    </div>
                </div>
            </div>
            <div class="wrapper-4">
                <img src="images/page-02_img01.jpg" alt="" width="1025" height="520">
            </div>
        </section>
        <!-- END For all your welding needs-->

        <!-- What is Welding and what do welders do? -->
        <section class="well-sm relative">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <h2>What is Welding and what do welders do?</h2>
                        <p class="inset-1">Welding is the most economical and efficient way to join metals permanently. It is the only way of joining two or more pieces of metal to make them act as a single piece. Welding is vital to our economy.</p>
                    </div>
                </div>
            </div>
            <div class="wrapper-3">
                <img src="images/page-02_img02.jpg" alt="" width="1025" height="520">
            </div>
        </section>
        <!-- END What is Welding and what do welders do?-->

        <!-- Pneumatic Engineering -->
        <section class="well-sm well-sm--inset-2 relative">
            <div class="container">
                <div class="row">
                    <div class="col-md-preffix-7 col-md-5">
                        <h2>Pneumatic Engineering</h2>
                        <ul class="marked-list">
                            <li><a href="#">Pneumatic Engineers</a></li>
                            <li><a href="#">Hydraulic Engineers</a></li>
                            <li><a href="#">Process Engineers</a></li>
                            <li><a href="#">Stainless Steel Pipework</a></li>
                            <li><a href="#">Carbon Steel Pipework Installation</a></li>
                            <li><a href="#">Pipework Fabrication</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="wrapper-4">
                <img src="images/page-02_img03.jpg" alt="" width="1025" height="520">
            </div>
        </section>
        <!-- END Pneumatic Engineering-->


        <!-- Network -->
        <section class="well-sm">
            <div class="container">
                <div class="row flow-offset-2">
                    <div class="col-sm-6">
                        <h2>Network</h2>
                        <p>We are constantly in touch with our network of customers, partners, employees and potential employees. In this network, the GodsFinger Engineering team always have specialists available for the successful realization of projects. As part of our network, you can always rely on actual information and a high level of service!</p>
                        <a href="#" class="btn btn-xl btn-primary">Read more</a>
                    </div>
                    <div class="col-sm-6">
                        <h2>Focus</h2>
                        <p>GodsFinger Engineering is focused on the deployment of expert teams of welders and fitters for new construction and maintenance projects in heavy industries such as; offshore, shipbuilding, petrochemicals, pharmaceuticals, food, feed and utilities. Because of this clear focus, we are specialists in our field of work.</p>
                        <a href="#" class="btn btn-xl btn-primary">Read more</a>
                    </div>
                </div>
            </div>
        </section>
        <!-- END Network-->
@endsection

{{--  scripts --}}
@section('scripts')
    
@endsection