@extends('layouts.web-skin')

{{--  title --}}
@section('title')
    GodsFinger Engineering | Products
@endsection

{{--  contents --}}
@section('contents')
    <!-- Feedback -->
    <section class="well-sm">
        <div class="container">
            <div class="row">
                <div class="col-md-6">

                    <table class="table">
                        <tr>
                            <td>Call Us:</td>
                            <td>
                                <a class="text-primary hover-effect-2" href="callto:#">+2347032378215</a>, 
                                <a class="text-primary hover-effect-2" href="callto:#">+2348177273900</a>
                                <br />
                                <a class="text-primary hover-effect-2" href="callto:#">+2348020702277</a>, 
                                <a class="text-primary hover-effect-2" href="callto:#">+2348055101983</a>
                            </td>
                        </tr>
                    </table>
                </div>

                <div class="col-md-6">
                    <table class="table">
                        <tr>
                            <td>Work/Office Address: </td>
                            <td>
                                 <a class="text-primary hover-effect-2" href="#">59 - 61 Mba Road, Ajegunle Olodi-Apapa, Lagos.</a>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </section>

    <section class="well-sm">
        <div class="container">
            <h2>Feedback</h2>
            <!-- RD Mailform -->
            <div class="rd-status"></div>
            <form class='rd-mailform' method="post" onsubmit="return sendContactMail()">
                <!-- RD Mailform Type -->
                <input type="hidden" name="form-type" value="contact"/>
                <!-- END RD Mailform Type -->
                <fieldset>
                    <div class="row">
                        <div class="col-md-4">
                            <label data-add-placeholder>
                                <input type="text"
                                       name="name" id="name"
                                       placeholder="Your name"
                                       data-constraints="@NotEmpty @LettersOnly"/>
                            </label>
                        </div>
                        <div class="col-md-4">
                            <label data-add-placeholder>
                                <input type="text"
                                       name="email" id="email"
                                       placeholder="Your email"
                                       data-constraints="@NotEmpty @Email"/>
                            </label>
                        </div>
                        <div class="col-md-4">
                            <label data-add-placeholder>
                                <input type="text"
                                       name="phone" id="phone"
                                       placeholder="Your phone"
                                       data-constraints="@Phone"/>
                            </label>
                        </div>
                    </div>

                    <label data-add-placeholder>
                        <textarea name="message" id="message" placeholder="Message"
                                  data-constraints="@NotEmpty"></textarea>
                    </label>

                    <div class="mfControls text-center text-md-left">
                        <button class="btn btn-xl btn-primary" id="send-btn" type="submit">Send</button>
                    </div>

                    <div class="mfInfo"></div>
                </fieldset>
            </form>
            <div class="rd-error"></div>
            <!-- END RD Mailform -->
        </div>
    </section>
    <!-- END Feedback -->
@endsection

{{--  scripts --}}
@section('scripts')
    <script type="text/javascript">
        // send contact mail
        function sendContactMail() {
            $("#send-btn").html('Sending...');
            var token   = $("#token").val();
            var email   = $("#email").val();
            var name    = $("#name").val();
            var phone   = $("#phone").val();
            var message = $("#message").val();

            var params = {
                _token: token,
                email: email,
                name: name,
                phone: phone,
                message: message
            };

            $.post('{{ url('send/contact/mail') }}', params, function(data, textStatus, xhr) {
                if(data.status == "success"){
                    $(".rd-mailform")[0].reset();
                    $(".rd-mailform").hide();
                    $(".rd-status").html(`
                        <h4>${data.message}</h4>
                    `);
                    $("#send-btn").html('Send');
                }else{
                    $(".rd-error").html(`
                        <h4>${data.message}</h4>
                    `);
                }
            }).fail(function (){
                alert('check internet access, failed to send message!');
            });

            // void
            return false;
        }
    </script>
@endsection