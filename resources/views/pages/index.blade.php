@extends('layouts.web-skin')

{{--  title --}}
@section('title')
    GodsFinger Engineering 
@endsection

{{--  contents --}}
@section('contents')
    <!-- Modern welding company -->
    <section class="gallery">
        <div class="wrapper-1">
            <div class="img-wrap-1">
                <img src="images/page-01_img01.jpg" alt="" width="1210" height="900">
            </div>
            <div class="wrapper-1__content">
                <h1>GodsFinger Engineering</h1>
                <a href="#" class="btn btn-xl btn-primary">Get estimate</a>
            </div>
        </div>

        <div class="wrapper-2" data-lightbox="gallery">
            <!-- Magnific Popup Image -->
            <a class="thumb mfp-image img-wrap-1" href="images/page-01_img02_original.jpg" data-lightbox="image">
                <img src="images/page-01_img02.jpg" alt="" width="840" height="300">
                <div class="thumb__overlay">
                    <h5 class="thumb__description">Custom <br class="hidden visible-lg-block">fabrication</h5>
                </div>
            </a>
            <!-- END Magnific Popup Image -->

            <!-- Magnific Popup Image -->
            <a class="thumb mfp-image img-wrap-1" href="images/page-01_img03_original.jpg" data-lightbox="image">
                <img src="images/page-01_img03.jpg" alt="" width="840" height="300">
                <div class="thumb__overlay">
                    <h5 class="thumb__description">We know metal</h5>
                </div>
            </a>
            <!-- END Magnific Popup Image -->

            <!-- Magnific Popup Image -->
            <a class="thumb mfp-image img-wrap-1" href="images/page-01_img04_original.jpg" data-lightbox="image">
                <img src="images/page-01_img04.jpg" alt="" width="840" height="300">
                <div class="thumb__overlay">
                    <h5 class="thumb__description">High quality work</h5>
                </div>
            </a>
            <!-- END Magnific Popup Image -->
        </div>
    </section>
    <!-- END Modern welding company-->
    
    <!-- Industry focus -->
    <section class="well-sm well-sm--inset-1 relative">
        <div class="container">
            <div class="row">
                <div class="col-md-5">
                    <h2>Our Services</h2>
                    <ul class="marked-list">
                        <li><a href="#">Advanced Manufacturing Equipments</a></li>
                        <li><a href="#">Gunstand With Balistic Shield</a></li>
                        <li><a href="#">Security Doors, Gates & Burglaries</a></li>
                        <li><a href="#">Hospital Equipment</a></li>
                        <li><a href="#">HouseHold/Office Equipment</a></li>
                        <li><a href="#">Bridal Events & Decorative Items</a></li>
                        <li><a href="#">Tents & Canopy</a></li>
                        <li><a href="#">Storage Tank & Overhead Tank Stand</a></li>
                        <li><a href="#">Truck Installations</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="wrapper-3">
            <img src="images/page-01_img05.jpg" alt="" width="1025" height="520">
        </div>
    </section>
    <!-- END Industry focus-->

    <!-- Our promise to you is what we provide -->
    <section class="well-sm well-sm--inset-2 relative">
        <div class="container">
            <div class="row">
                <div class="col-md-preffix-7 col-md-5">
                    <h2>Our promise to you is what we provide</h2>
                    <ul class="marked-list">
                        <li><a href="#">High quality work</a></li>
                        <li><a href="#">Excellent aftercare service</a></li>
                        <li><a href="#">Superior standards of workmanship</a></li>
                        <li><a href="#">Delivery on time</a></li>
                        <li><a href="#">Truck & trailer repair</a></li>
                    </ul>
                    <a href="#" class="btn btn-xl btn-primary">Get estimate</a>
                </div>
            </div>
        </div>
        <div class="wrapper-5">
            <div class="img-wrap-1"><img src="images/page-01_img06.jpg" alt="" width="1025" height="490"></div>
            <div class="content"><h2>Pick your service</h2></div>
        </div>
    </section>
    <!-- END Our promise to you is what we provide-->

    <!-- Carousel -->
    <section class="bg-grayscale-default">
        <!-- Owl Carousel -->
        <div class="owl-carousel"
             data-items="1"
             data-xs-items="2"
             data-md-items="3"
             data-lg-items="5"
             data-stage-padding="15"
             data-lg-stage-padding="0"
             data-lightbox="gallery">
            <div class="owl-item">
                <a class="thumb thumb-mod-1 mfp-image img-wrap-1 vertical-limiter-1" href="images/page-01_img07_original.jpg" data-lightbox="image">
                    <img src="images/page-01_img07.jpg" alt="" width="410" height="580">
                    <div class="thumb__overlay">
                        <h5 class="thumb__description">MIG welding</h5>
                    </div>
                </a>
            </div>
            <div class="owl-item">
                <a class="thumb thumb-mod-1 mfp-image img-wrap-1 vertical-limiter-1" href="images/page-01_img08_original.jpg" data-lightbox="image">
                    <img src="images/page-01_img08.jpg" alt="" width="410" height="580">
                    <div class="thumb__overlay">
                        <h5 class="thumb__description">TIG welding</h5>
                    </div>
                </a>
            </div>
            <div class="owl-item">
                <a class="thumb thumb-mod-1 mfp-image img-wrap-1 vertical-limiter-1" href="images/page-01_img09_original.jpg" data-lightbox="image">
                    <img src="images/page-01_img09.jpg" alt="" width="410" height="580">
                    <div class="thumb__overlay">
                        <h5 class="thumb__description">Stick welding</h5>
                    </div>
                </a>
            </div>
            <div class="owl-item">
                <a class="thumb thumb-mod-1 mfp-image img-wrap-1 vertical-limiter-1" href="images/page-01_img10_original.jpg" data-lightbox="image">
                    <img src="images/page-01_img10.jpg" alt="" width="410" height="580">
                    <div class="thumb__overlay">
                        <h5 class="thumb__description">Plasma cutting</h5>
                    </div>
                </a>
            </div>
            <div class="owl-item">
                <a class="thumb thumb-mod-1 mfp-image img-wrap-1 vertical-limiter-1" href="images/page-01_img11_original.jpg" data-lightbox="image">
                    <img src="images/page-01_img11.jpg" alt="" width="410" height="580">
                    <div class="thumb__overlay">
                        <h5 class="thumb__description">Metal Fabrication</h5>
                    </div>
                </a>
            </div>
        </div>
        <!-- END Owl Carousel -->
    </section>
    <!-- END Carousel-->

    <!-- Testimonials -->
    <section class="bg-default offset-5">
        <div class="container">
            <div class="row">
                <div class="col-md-5 well-sm well-sm--inset-1 bg-primary stretch-left">
                    <h2>Testimonials</h2>
                </div>
                <div class="col-md-7 well-lg well-lg--inset-1 offset-0">
                    <!-- Owl Carousel -->
                    <div class="owl-carousel carousel-1"
                         data-nav="true"
                         data-dots="true">
                        <div class="owl-item">
                            <blockquote class="quote">
                                <p><q>Hi. Last month you made a railing for us. Well, we have finally got it in place. The garden is still not finished, but it is starting to look very nice and everyone loves the railing! It looks like it came with the house. <br class="hidden visible-lg-block">Thanks a lot!</q></p>
                                <p class="link"><cite><a href="#">Mary Simmons</a></cite></p>
                            </blockquote>
                        </div>
                        <div class="owl-item">
                            <blockquote class="quote">
                                <p><q>Thank-you so much for all the help you provided before, during, and after I ordered railings from you and also for your flexibility around delivery and / or collection. I am very pleased with the finished product and it looks great in situ. Thank-you for such a professional, yet friendly, service.</q></p>
                                <p class="link"><cite><a href="#">Karen Richardson</a></cite></p>
                            </blockquote>
                        </div>
                    </div>
                    <!-- END Owl Carousel -->
                </div>
            </div>
        </div>
    </section>
    <!-- END Testimonials-->

    <!-- RD Google Map -->
    <section class="rd-google-map">
        <div id="google-map" class="rd-google-map__model" data-zoom="14" data-x="-73.9874068" data-y="40.643180"></div>
        <ul class="rd-google-map__locations">
            <li data-x="-73.9874068" data-y="40.643180">
                <p>59 - 61 Mba Road, Ajegunle Olodi-Apapa, Lagos.</p>
                <p class="text-ubold">
                    Contact us with any of the followings: <br /> 
                    +234 08020702277<br />
                    +234 08055101983<br />
                    +234 08177273900<br />
                    +234 07032378215<br />
                </p>
            </li>
        </ul>
    </section>
    <!-- END RD Google Map -->
@endsection

{{--  scripts --}}
@section('scripts')
    
@endsection