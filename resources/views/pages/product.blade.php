@extends('layouts.web-skin')

{{--  title --}}
@section('title')
    GodsFinger Engineering | Products
@endsection

{{--  contents --}}
@section('contents')
    

    <!-- What is Welding and what do welders do? -->
    <section class="well-sm relative">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>Previous Works</h2>
                    <hr />

                    <div class="row">
                        <div class="col-md-2">
                            <img style="box-shadow: 1px 1px 4px 1px #CCC;border-radius: 5px;" src="{{ asset('images/products/01.jpeg') }}">
                        </div>
                        <div class="col-md-2">
                            <img style="box-shadow: 1px 1px 4px 1px #CCC;border-radius: 5px;" src="{{ asset('images/products/02.jpeg') }}">
                        </div>
                        <div class="col-md-2">
                            <img style="box-shadow: 1px 1px 4px 1px #CCC;border-radius: 5px;" src="{{ asset('images/products/03.jpeg') }}">
                        </div>
                        <div class="col-md-2">
                            <img style="box-shadow: 1px 1px 4px 1px #CCC;border-radius: 5px;" src="{{ asset('images/products/04.jpeg') }}">
                        </div>
                        <div class="col-md-2">
                            <img style="box-shadow: 1px 1px 4px 1px #CCC;border-radius: 5px;" src="{{ asset('images/products/05.jpeg') }}">
                        </div>
                        <div class="col-md-2">
                            <img style="box-shadow: 1px 1px 4px 1px #CCC;border-radius: 5px;" src="{{ asset('images/products/06.jpeg') }}">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2">
                            <img style="box-shadow: 1px 1px 4px 1px #CCC;border-radius: 5px;" src="{{ asset('images/products/07.jpeg') }}">
                        </div>
                        <div class="col-md-2">
                            <img style="box-shadow: 1px 1px 4px 1px #CCC;border-radius: 5px;" src="{{ asset('images/products/08.jpeg') }}">
                        </div>
                        <div class="col-md-2">
                            <img style="box-shadow: 1px 1px 4px 1px #CCC;border-radius: 5px;" src="{{ asset('images/products/09.jpeg') }}">
                        </div>
                        <div class="col-md-2">
                            <img style="box-shadow: 1px 1px 4px 1px #CCC;border-radius: 5px;" src="{{ asset('images/products/10.jpeg') }}">
                        </div>
                        <div class="col-md-2">
                            <img style="box-shadow: 1px 1px 4px 1px #CCC;border-radius: 5px;" src="{{ asset('images/products/11.jpeg') }}">
                        </div>
                        <div class="col-md-2">
                            <img style="box-shadow: 1px 1px 4px 1px #CCC;border-radius: 5px;" src="{{ asset('images/products/12.jpeg') }}">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2">
                            <img style="box-shadow: 1px 1px 4px 1px #CCC;border-radius: 5px;" src="{{ asset('images/products/13.jpeg') }}">
                        </div>
                        <div class="col-md-2">
                            <img style="box-shadow: 1px 1px 4px 1px #CCC;border-radius: 5px;" src="{{ asset('images/products/14.jpeg') }}">
                        </div>
                        <div class="col-md-2">
                            <img style="box-shadow: 1px 1px 4px 1px #CCC;border-radius: 5px;" src="{{ asset('images/products/15.jpeg') }}">
                        </div>
                        <div class="col-md-2">
                            <img style="box-shadow: 1px 1px 4px 1px #CCC;border-radius: 5px;" src="{{ asset('images/products/16.jpeg') }}">
                        </div>
                        <div class="col-md-2">
                            <img style="box-shadow: 1px 1px 4px 1px #CCC;border-radius: 5px;" src="{{ asset('images/products/17.jpeg') }}">
                        </div>
                        <div class="col-md-2">
                            <img style="box-shadow: 1px 1px 4px 1px #CCC;border-radius: 5px;" src="{{ asset('images/products/18.jpeg') }}">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2">
                            <img style="box-shadow: 1px 1px 4px 1px #CCC;border-radius: 5px;" src="{{ asset('images/products/19.jpeg') }}">
                        </div>
                        <div class="col-md-2">
                            <img style="box-shadow: 1px 1px 4px 1px #CCC;border-radius: 5px;" src="{{ asset('images/products/20.jpeg') }}">
                        </div>
                        <div class="col-md-2">
                            <img style="box-shadow: 1px 1px 4px 1px #CCC;border-radius: 5px;" src="{{ asset('images/products/21.jpeg') }}">
                        </div>
                        <div class="col-md-2">
                            <img style="box-shadow: 1px 1px 4px 1px #CCC;border-radius: 5px;" src="{{ asset('images/products/22.jpeg') }}">
                        </div>
                        <div class="col-md-2">
                            <img style="box-shadow: 1px 1px 4px 1px #CCC;border-radius: 5px;" src="{{ asset('images/products/23.jpeg') }}">
                        </div>
                        <div class="col-md-2">
                            <img style="box-shadow: 1px 1px 4px 1px #CCC;border-radius: 5px;" src="{{ asset('images/products/24.jpeg') }}">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2">
                            <img style="box-shadow: 1px 1px 4px 1px #CCC;border-radius: 5px;" src="{{ asset('images/products/25.jpeg') }}">
                        </div>
                        <div class="col-md-2">
                            <img style="box-shadow: 1px 1px 4px 1px #CCC;border-radius: 5px;" src="{{ asset('images/products/26.jpeg') }}">
                        </div>
                        <div class="col-md-2">
                            <img style="box-shadow: 1px 1px 4px 1px #CCC;border-radius: 5px;" src="{{ asset('images/products/27.jpeg') }}">
                        </div>
                        <div class="col-md-2">
                            <img style="box-shadow: 1px 1px 4px 1px #CCC;border-radius: 5px;" src="{{ asset('images/products/28.jpeg') }}">
                        </div>
                        <div class="col-md-2">
                            <img style="box-shadow: 1px 1px 4px 1px #CCC;border-radius: 5px;" src="{{ asset('images/products/41.jpeg') }}">
                        </div>
                        <div class="col-md-2">
                            <img style="box-shadow: 1px 1px 4px 1px #CCC;border-radius: 5px;" src="{{ asset('images/products/30.jpeg') }}">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2">
                            <img style="box-shadow: 1px 1px 4px 1px #CCC;border-radius: 5px;" src="{{ asset('images/products/31.jpeg') }}">
                        </div>
                        <div class="col-md-2">
                            <img style="box-shadow: 1px 1px 4px 1px #CCC;border-radius: 5px;" src="{{ asset('images/products/32.jpeg') }}">
                        </div>
                        <div class="col-md-2">
                            <img style="box-shadow: 1px 1px 4px 1px #CCC;border-radius: 5px;" src="{{ asset('images/products/33.jpeg') }}">
                        </div>
                        <div class="col-md-2">
                            <img style="box-shadow: 1px 1px 4px 1px #CCC;border-radius: 5px;" src="{{ asset('images/products/34.jpeg') }}">
                        </div>
                        <div class="col-md-2">
                            <img style="box-shadow: 1px 1px 4px 1px #CCC;border-radius: 5px;" src="{{ asset('images/products/35.jpeg') }}">
                        </div>
                        <div class="col-md-2">
                            <img style="box-shadow: 1px 1px 4px 1px #CCC;border-radius: 5px;" src="{{ asset('images/products/40.jpeg') }}">
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <img style="box-shadow: 1px 1px 4px 1px #CCC;border-radius: 5px;" src="{{ asset('images/products/42.jpeg') }}">
                        </div>
                        <div class="col-md-6">
                            <img style="box-shadow: 1px 1px 4px 1px #CCC;border-radius: 5px;" src="{{ asset('images/products/43.jpeg') }}">
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>
    <!-- END What is Welding and what do welders do?-->  


     <!-- What is Welding and what do welders do? -->
    <section class="well-sm relative">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h2>What is Welding and what do welders do?</h2>
                    <p class="inset-1">Welding is the most economical and efficient way to join metals permanently. It is the only way of joining two or more pieces of metal to make them act as a single piece. Welding is vital to our economy. </p>
                </div>
            </div>
        </div>
        <div class="wrapper-3">
            <img src="images/page-02_img02.jpg" alt="" width="1025" height="520">
        </div>
    </section>
    <!-- END What is Welding and what do welders do?-->  

    <!-- Our products -->
    <section class="well-sm well-sm--inset-3">
        <div class="container">
            <h2>Our products</h2>
            <div class="isotope-filters">
                <button class="active" data-isotope-filter="*" data-isotope-group="gallery">Show All</button>
                <button data-isotope-filter="type-1" data-isotope-group="gallery">Category 1</button>
                <button data-isotope-filter="type-2" data-isotope-group="gallery">Category 2</button>
                <button data-isotope-filter="type-3" data-isotope-group="gallery">Category 3</button>
            </div>
        </div>

        <div class="row row-no-gutter isotope inset-3" data-isotope-group="gallery" data-isotope-layout="masonry" data-lightbox="gallery">
            <div class="col-sm-6 col-md-4" data-filter="type-1">
                <!-- Magnific Popup Image -->
                <a class="thumb thumb-mod-2 mfp-image vertical-limiter-2" href="images/page-04_img01_original.jpg" data-lightbox="image">
                    <img src="images/page-04_img01.jpg" alt="" width="684" height="596">
                    <div class="thumb__overlay"><div class="thumb__description">GTAW or Tungsten Inert Gas</div></div>
                </a>
                <!-- END Magnific Popup Image -->
            </div>

            <div class="col-sm-6 col-md-4" data-filter="type-3">
                <!-- Magnific Popup Image -->
                <a class="thumb thumb-mod-2 mfp-image vertical-limiter-2" href="images/page-04_img03_original.jpg" data-lightbox="image">
                    <img src="images/page-04_img03.jpg" alt="" width="684" height="776">
                    <div class="thumb__overlay"><div class="thumb__description">GMAW or Gas Metal Arc Welding</div></div>
                </a>
                <!-- END Magnific Popup Image -->
            </div>

            <div class="col-sm-6 col-md-4" data-filter="type-1">
                <!-- Magnific Popup Image -->
                <a class="thumb thumb-mod-2 mfp-image vertical-limiter-2" href="images/page-04_img05_original.jpg" data-lightbox="image">
                    <img src="images/page-04_img05.jpg" alt="" width="684" height="596">
                    <div class="thumb__overlay"><div class="thumb__description">Arc Welding or SMAW</div></div>
                </a>
                <!-- END Magnific Popup Image -->
            </div>

            <div class="col-sm-6 col-md-4" data-filter="type-2">
                <!-- Magnific Popup Image -->
                <a class="thumb thumb-mod-2 mfp-image vertical-limiter-2" href="images/page-04_img02_original.jpg" data-lightbox="image">
                    <img src="images/page-04_img02.jpg" alt="" width="684" height="866">
                    <div class="thumb__overlay"><div class="thumb__description">Gas or Oxy Acetylene Welding And Cutting</div></div>
                </a>
                <!-- END Magnific Popup Image -->
            </div>

            <div class="col-sm-6 col-md-4" data-filter="type-2">
                <!-- Magnific Popup Image -->
                <a class="thumb thumb-mod-2 mfp-image vertical-limiter-2" href="images/page-04_img06_original.jpg" data-lightbox="image">
                    <img src="images/page-04_img06.jpg" alt="" width="684" height="866">
                    <div class="thumb__overlay"><div class="thumb__description">Welders Protective Gear</div></div>
                </a>
                <!-- END Magnific Popup Image -->
            </div>

            <div class="col-sm-6 col-md-4" data-filter="type-3">
                <!-- Magnific Popup Image -->
                <a class="thumb thumb-mod-2 mfp-image vertical-limiter-2" href="images/page-04_img04_original.jpg" data-lightbox="image">
                    <img src="images/page-04_img04.jpg" alt="" width="684" height="686">
                    <div class="thumb__overlay"><div class="thumb__description">Welding Machines</div></div>
                </a>
                <!-- END Magnific Popup Image -->
            </div>

        </div>
    </section>
    <!-- END Our products-->
    
    <!-- Testimonials -->
    <section class="bg-default offset-4">
        <div class="container text-center text-md-left">
            <div class="row">
                <div class="col-md-5 well-sm bg-primary stretch-left">
                    <h2>Welding <br class="hidden visible-lg-block"> & cutting equipment</h2>
                </div>
                <div class="col-md-7 well-md offset-0">
                    <!-- Owl Carousel -->
                    <div class="owl-carousel carousel-2"
                         data-nav="true"
                         data-dots="true">
                        <div class="owl-item">
                            <div class="inset-2">
                                <h6>WELDING EQUIPMENT</h6>
                                <p>Welding equipment includes stick welders, tig welders, MIG welders, multi-process welders, advanced process welders, multi-operator welders, engine drives, submerged arc equipment, and wire feeders for arc welding.</p>
                                <a href="#" class="btn btn-xl btn-primary">Read more</a>
                            </div>
                        </div>
                        <div class="owl-item">
                            <div class="inset-2">
                                <h6>CUTTING EQUIPMENT</h6>
                                <p>Cutting equipment includes plasma cutters, plasma cutting systems, CNC cutting systems, pipe cutting machines, structural steel fabrication, motion and shape cutting controllers, and genuine torch consumables for plasma torches.</p>
                                <a href="#" class="btn btn-xl btn-primary">Read more</a>
                            </div>
                        </div>
                    </div>
                    <!-- END Owl Carousel -->
                </div>
            </div>
        </div>
    </section>
    <!-- END Testimonials-->
@endsection

{{--  scripts --}}
@section('scripts')
    
@endsection