<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 						'ExternalPagesController@index');
Route::get('/product', 					'ExternalPagesController@product');
Route::get('/about', 					'ExternalPagesController@about');
Route::get('/contact', 					'ExternalPagesController@contact');
Route::get('/terms', 					'ExternalPagesController@terms');
Route::get('/policy', 					'ExternalPagesController@policy');


Route::post('/send/contact/mail',		'JsonResponseController@sendMail');
